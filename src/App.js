import React from 'react';
import axios from 'axios';
import './App.css';
import Product from './Components/Product';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import SingleProduct from './Components/SingleProduct';
import NewProduct from './Components/NewProduct';

class App extends React.Component {

  constructor(){

    super();

    this.API_STATES = {
      LOADING: 'loading',
      LOADED: 'loaded',
      ERROR: 'error'
    };

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: ''
    };

    this.URL = 'https://fakestoreapi.com/products';
  }

  handleUpdate = (title,category,description,price,image,productID) => {

    let updatedProducts = this.state.products;

    let updatedProduct = updatedProducts.find((product) => {
      return product.id === productID;
    });

    if(title !== ''){
      updatedProduct.title = title;
    }
    if(category !== ''){
      updatedProduct.category = category;
    }
    if(description !== ''){
      updatedProduct.description = description;
    }
    if(price !== ''){
      updatedProduct.price = price;
    }

    if(image !== ''){
      updatedProduct.image = image;
    }

    this.setState({
      products: updatedProducts
    });

  }

  handleDelete = (productID) => {

    let newProducts = this.state.products.filter((product) => {
      return product.id !== productID;
    });

    this.setState({
      products: newProducts
    });
  }

  handleCreate = (title,category,description,price,image) => {

    let newProduct = {};

    newProduct.id = this.state.products[this.state.products.length-1].id + 1;
    newProduct.title = title;
    newProduct.price = price;
    newProduct.description = description;
    newProduct.category = category;
    newProduct.image = image;
    newProduct.rating = {
      rate: 0,
      count: 0
    };

    let newProducts = this.state.products;
    newProducts.push(newProduct);

    this.setState({
      products: newProducts
    });
  }

  fetchProductData = (url) => {

    axios.get(url)
    .then((response) => {

      this.setState({
        products: response.data,
        status: this.API_STATES.LOADED
      });
    })
    .catch((err) => {
      
      this.setState({
        status: this.API_STATES.ERROR,
        errorMessage: 'API error occurred. Please try again!'
      });
    });
    
  }

  componentDidMount(){

    this.fetchProductData(this.URL);

  }

  render(){

    return(
      <>
        {this.state.status === this.API_STATES.ERROR && 
          <h1>{this.state.errorMessage}</h1>
        }

        {this.state.status === this.API_STATES.LOADING && 
          <div className='loader'>
            <div className="lds-ripple"><div></div><div></div></div>
          </div>
        }

        {this.state.status === this.API_STATES.LOADED && this.state.products.length === 0 && 
          <h1>Sorry. No products available !</h1>
        }

        {this.state.status === this.API_STATES.LOADED && this.state.products.length > 0 &&
          <Router>
            <div className='mainPage'>
              <nav className="navbar navbar-expand-lg navbar-light headerNav ps-5">
                <h1 className="pageTitle">FAKE STORE</h1>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    <li className="nav-item active">
                      <Link to='/' className="nav-link text-white">HOME<span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item">
                      <Link to='/new' className="nav-link text-white">CREATE NEW PRODUCT</Link>
                    </li>
                  </ul>
                </div>
              </nav>
              <div className="mainPageContent">
                <Switch>

                  <Route path='/' exact>
                    {this.state.products.map((product) => {
                      return (
                      <Product key={product.id} id={product.id} title={product.title} description={product.description} category={product.category} rating={product.rating} price={product.price} image={product.image} handleDelete={() => {
                        this.handleDelete(product.id)
                        }}/>
                      )
                    })}
                  </Route>

                  <Route path='/new'><NewProduct handleCreate={this.handleCreate} exact/></Route>

                  <Route path='/:id' render={(routeProps) => {
                    return <SingleProduct routeProps={routeProps} products={this.state.products} handleUpdate={this.handleUpdate}/>
                  }} exact />
                  
                </Switch>
              </div>
            </div>
          </Router>
        }
      </>
    );
  }
}

export default App;
