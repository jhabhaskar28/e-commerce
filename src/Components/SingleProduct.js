import React from "react";
import '../SingleProduct.css';
import validator from 'validator';

class SingleProduct extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            title: '',
            category: '',
            description: '',
            price: '',
            image: '',
            isTitleChanged: false,
            isDescriptionChanged: false,
            isPriceChanged: false,
            isImageChanged: false,
            isProductUpdated: false
        };
    }

    validateUpdate = (event,productID) => {

        event.preventDefault();

        if(((this.state.title.trim().length !== 0 && this.state.isTitleChanged) || !this.state.isTitleChanged) && 
        ((this.state.description.trim().length !== 0 && this.state.isDescriptionChanged) || !this.state.isDescriptionChanged) &&
        ((this.state.image.trim().length !== 0 && this.state.isImageChanged) || !this.state.isImageChanged) &&  
        ((validator.isNumeric(this.state.price) && this.state.isPriceChanged) || !this.state.isPriceChanged)){

            this.props.handleUpdate(this.state.title,this.state.category,this.state.description,this.state.price,this.state.image,productID);
            this.setState({
                isProductUpdated: true
            });
        } else {
            this.setState({
                isProductUpdated: false
            });
        }
    }

    render(){
        
        let singleProduct = this.props.products.filter((product) => {
            return product.id === parseInt(this.props.routeProps.match.params.id);
        });

        return(
            <div>
                <header className="singleProductHeader">
                    <h1>UPDATE PRODUCT</h1>
                    {this.state.isProductUpdated && <h4 className="updatedProductHeading">Product Updated</h4>}
                </header>

                <div className='container singleProduct p-5'>
                    <form>
                        <div className="form-group row mb-3">
                            <label htmlFor="inputTitle" className="col-sm-2 col-form-label">Title:</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputTitle" placeholder="Enter Title" defaultValue={singleProduct[0].title} onChange={(event) => {
                                    this.setState({
                                        title: event.target.value,
                                        isTitleChanged: true
                                    });
                                }}/>
                                {this.state.title.trim().length === 0 && this.state.isTitleChanged && <small className="form-text text-danger">Please enter a valid title</small>}
                            </div>
                        </div>
                        <div className="form-group row mb-3">
                            <label htmlFor="categorySelect" className="col-sm-2 col-form-label">Category:</label>
                            <div className="col-sm-10">
                                <select className="form-control" id="categorySelect" defaultValue={singleProduct[0].category} onChange={(event) => {
                                    this.setState({
                                        category: event.target.value,
                                    });
                                }}>
                                    <option>men's clothing</option>
                                    <option>women's clothing</option>
                                    <option>electronics</option>
                                    <option>jewelery</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group row mb-3">
                            <label htmlFor="inputDescription" className="col-sm-2 col-form-label">Description:</label>
                            <div className="col-sm-10">
                                <textarea type="text" className="form-control descriptionInput" id="inputDescription" placeholder="Enter Description" defaultValue={singleProduct[0].description} onChange={(event) => {
                                    this.setState({
                                        description: event.target.value,
                                        isDescriptionChanged: true
                                    });
                                }}/>
                                {this.state.description.trim().length === 0 && this.state.isDescriptionChanged && <small className="form-text text-danger">Please enter a valid description</small>}
                            </div>
                        </div>
                        <div className="form-group row mb-3">
                            <label htmlFor="inputPrice" className="col-sm-2 col-form-label">Price:</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputPrice" placeholder="Enter Price" defaultValue={singleProduct[0].price} onChange={(event) => {
                                    this.setState({
                                        price: event.target.value,
                                        isPriceChanged: true
                                    });
                                }}/>
                                {!validator.isNumeric(this.state.price) && this.state.isPriceChanged && <small className="form-text text-danger">Please enter a valid price</small>}
                            </div>
                        </div>
                        <div className="form-group row mb-3">
                            <label htmlFor="inputImage" className="col-sm-2 col-form-label">Image URL:</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputImage" placeholder="Enter Image URL" defaultValue={singleProduct[0].image} onChange={(event) => {
                                    this.setState({
                                        image: event.target.value,
                                        isImageChanged: true
                                    });
                                }}/>
                                {this.state.image.trim().length === 0 && this.state.isImageChanged && <small className="form-text text-danger">Image URL cannot be empty</small>}
                            </div>
                        </div>
                        <div className="form-group row mt-5">
                                <button type="submit" className="btn btn-primary" onClick={(event) => {
                                    this.validateUpdate(event,singleProduct[0].id);
                                }}>Update Product</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default SingleProduct;