import React from "react";
import '../Product.css';
import {Link} from 'react-router-dom';

class Product extends React.Component{
    
    constructor(props){
        
        super(props);

        this.state = {
            isDeleteButtonClicked: false        
        };
    }

    render(){

        return (

            <div className="product">
                <section className="productImage">
                    <img src={this.props.image} alt="Product"></img>
                </section>

                <section className="productDetails">
                    <h3>{this.props.title}</h3>
                    <h5><i className="fa-solid fa-star"></i> {this.props.rating.rate}<span> ({this.props.rating.count})</span></h5>
                    <h6>Category: {this.props.category}</h6>
                    <p>{this.props.description}</p>
                </section>
                
                <section className="productPrice">
                    <h2>Price: ${this.props.price}</h2>
                    <Link to={`/${this.props.id}`}><button className="updateButton updatedButton"><i className="fa-solid fa-pen-to-square"></i> Update</button></Link>
                    <button onClick={() => {
                        this.setState({
                            isDeleteButtonClicked: true
                        });
                    }} className="deleteButton updatedButton"><i className="fa-solid fa-trash"></i> Delete</button>
                    
                    {this.state.isDeleteButtonClicked===true &&
                        <div className="deleteConfirm">
                            <p>Delete product?</p>
                            <button onClick={this.props.handleDelete} className="redButton">Yes</button>
                            <button onClick={() => {
                                this.setState({
                                    isDeleteButtonClicked: false
                                });
                            }} className="greenButton">No</button>
                        </div>
                    }

                </section>
            </div>
        );
    }
}

export default Product;