import React from "react";
import '../NewProduct.css';
import validator from 'validator';

class NewProduct extends React.Component{

    constructor(props){

        super(props);

        this.state = {
            title: '',
            category: '',
            description: '',
            price: '',
            image: '',

            formError: {
                titleError: '',
                categoryError: '',
                descriptionError: '',
                priceError: ''
            },
            isFormError: true,
        };
    }

    validateCreate = (event) => {

        let formErrors = {};
        event.preventDefault();

        if(this.state.title.trim().length === 0){
            formErrors.titleError = "Please enter a valid title.";
        }

        if(this.state.category === '' || this.state.category === 'Select product category'){
            formErrors.categoryError = "Please select product category";
        }

        if(this.state.description.trim().length === 0){
            formErrors.descriptionError = "Please enter a valid description.";
        }

        if(!validator.isNumeric(this.state.price)){
            formErrors.priceError = "Please enter a valid price.";
        }

        if(Object.keys(formErrors).length === 0){

            this.props.handleCreate(this.state.title,this.state.category,this.state.description,this.state.price,this.state.image);

            this.setState({
                title: '',
                category: '',
                description: '',
                price: '',
                image: '',

                formError: {
                    titleError: '',
                    categoryError: '',
                    descriptionError: '',
                    priceError: ''
                },
                isFormError: false

            });

        } else {
            this.setState({
                formError: formErrors
            });
        }

    }

    render(){
        return(
            <div>
                <header className="newProductTitle">
                    <h2>CREATE NEW PRODUCT</h2>
                    {!this.state.isFormError && <h4 className="productCreated">Product Created</h4>}
                </header>

                <div className="container newProduct p-5">
                    <form>
                        <div className="form-group row mb-3">
                            <label htmlFor="inputTitle" className="col-sm-2 col-form-label">Title</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputTitle" placeholder="Enter title" value={this.state.title} onChange={(event) => {
                                    this.setState({
                                        title: event.target.value,
                                    });
                                }}/>
                                <small className="form-text text-danger">{this.state.formError.titleError}</small>
                            </div>
                        </div>
                        
                        <div className="form-group row mb-3">
                            <label htmlFor="categorySelect" className="col-sm-2 col-form-label">Category</label>
                            <div className="col-sm-10">
                                <select className="form-control" id="categorySelect" value={this.state.category} onChange={(event) => {
                                    this.setState({ 
                                        category: event.target.value,
                                    });
                                }}>
                                    <option>Select product category</option>
                                    <option>men's clothing</option>
                                    <option>women's clothing</option>
                                    <option>electronics</option>
                                    <option>jewelery</option>
                                </select>
                                <small className="form-text text-danger">{this.state.formError.categoryError}</small>
                            </div>
                        </div>

                        <div className="form-group row mb-3">
                            <label htmlFor="inputDescription" className="col-sm-2 col-form-label">Description</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputDescription" placeholder="Enter Description" value={this.state.description} onChange={(event) => {
                                    this.setState({
                                        description: event.target.value,
                                    });
                                }}/>
                                <small className="form-text text-danger">{this.state.formError.descriptionError}</small>
                            </div>
                        </div>

                        <div className="form-group row mb-3">
                            <label htmlFor="inputPrice" className="col-sm-2 col-form-label">Price</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputPrice" placeholder="Enter Price" value={this.state.price} onChange={(event) => {
                                    this.setState({
                                        price: event.target.value,
                                    });
                                }}/>
                                <small className="form-text text-danger">{this.state.formError.priceError}</small>
                            </div>
                        </div>


                        <div className="form-group row mb-3">
                            <label htmlFor="inputImage" className="col-sm-2 col-form-label">Product Image URL:(Optional)</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" id="inputImage" placeholder="Enter Image URL" value={this.state.image} onChange={(event) => {
                                    this.setState({
                                        image: event.target.value,
                                    });
                                }}/>
                            </div>
                        </div>
        
                        <div className="form-group row mt-5">
                                <button type="submit" className="btn btn-primary" onClick={(event) => {
                                    this.validateCreate(event);
                                }}>Create Product</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default NewProduct;